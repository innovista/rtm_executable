﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using Innovista.RealTimeMessaging.Engine;

namespace AvailityXmlClient
{
	class Program
	{
		static void Main(string[] args)
		{
			Parser.Default.ParseArguments<ProgramOptions>(args)
				.WithParsed<ProgramOptions>(o =>
				{
					if (o.ConnectionString == null)
						o.ConnectionString = ProgramConfig.AppConnection;

                    if (!String.IsNullOrEmpty(o.PasswordToEncrypt))
                        new RealTimeMessageProcessing(o).EncryptPassword();

					new RealTimeMessageProcessing(o).Process();
				});
		}
	}
}
