﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace Innovista.Payer.Connection
{
	public class SampleClient
	{
		public string Url { get; set; }
		public string UserName { get; set; }
		/// <summary>
		/// Send 270/276 transaction to Aries realtime web service
		/// </summary>
		/// <param name="payloadId">unique identifier</param>
		/// <param name="payload">ANSI 270 or 276 request</param>
		/// <param name="transactionCode">transaction code (either '270' or '276')</param>
		/// <param name="receiverCode">AVAILITY</param>
		/// <returns>ANSI 271 or 277 response</returns>
		public string Send(string payloadId, string payload, string transactionCode, string receiverCode)
		{
			string ansiResponse = "";
			try
			{
				var xd = new XmlDocument();
				xd.LoadXml("<?xml version=\"1.0\"?><ENVELOPE/>");
				var xnHeader = xd.DocumentElement.AppendChild(xd.CreateElement("HEADER"));
				xnHeader.AppendChild(xd.CreateElement("TranCode")).InnerText = transactionCode;
				xnHeader.AppendChild(xd.CreateElement("MessageFormat")).InnerText = "X12";
				xnHeader.AppendChild(xd.CreateElement("Sender")).InnerText = UserName;
				xnHeader.AppendChild(xd.CreateElement("Receiver")).InnerText = receiverCode;
				xnHeader.AppendChild(xd.CreateElement("Session"));
				xnHeader.AppendChild(xd.CreateElement("ProviderOfficeNbr")).InnerText = "office_number_here";
				xnHeader.AppendChild(xd.CreateElement("ProviderTransID")).InnerText = payloadId;
				xd.DocumentElement.AppendChild(xd.CreateElement("BODY")).InnerText = payload;
				var httpRequest = (HttpWebRequest)WebRequest.Create(Url);
				httpRequest.Method = "POST";
				httpRequest.ContentType = "application/x-www-form-urlencoded";
				var xmlRequest = xd.OuterXml.Replace("<Session />", "<Session></Session>");
				SendRequest(httpRequest, xmlRequest);
				var xmlResponse = GetResponse(httpRequest);
				if (!string.IsNullOrEmpty(xmlResponse))
				{
					xd.LoadXml(xmlResponse);
					var xnTemp = xd.DocumentElement.SelectSingleNode("BODY");
					if (xnTemp != null)
					{
						var xnError = xnTemp.SelectSingleNode("Interchange/Error");
						if (xnError != null)
						{
							ansiResponse = xnError.InnerText;
						}
						else
						{
							ansiResponse = xnTemp.InnerText;
						}
					}
				}
				return ansiResponse;
			}
			catch (Exception ex)
			{
			}
		}
		protected virtual void SendRequest(HttpWebRequest request, string payload)
		{
			using (var writer = new StreamWriter(request.GetRequestStream(), Encoding.Default))
			{
				writer.Write(payload);
			}
		}
		protected virtual string GetResponse(HttpWebRequest request)
		{
			var response = (HttpWebResponse)request.GetResponse();
			using (var reader = new StreamReader(response.GetResponseStream()))
			{
				return reader.ReadToEnd();
			}
		}
	}
}
