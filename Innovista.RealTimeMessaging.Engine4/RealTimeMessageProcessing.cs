﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innovista.Common;
using Innovista.RealTimeMessaging.Data;
using System.Data;

namespace Innovista.RealTimeMessaging.Engine
{
	public enum SupportedProviderEnum
	{
		Availity = 1,
		AvailityCore = 2
	}
	public class RealTimeMessageProcessing
	{
		private ProgramOptions curOptions { get; set; }
        Dictionary<int, List<ErrorMessage>> errMessageDict = new Dictionary<int, List<ErrorMessage>>();


        public RealTimeMessageProcessing(ProgramOptions runOptions)
		{
			curOptions = runOptions;

			if (curOptions.RunMode == null)
				curOptions.RunMode = ProcessRunMode.Test;

            if (curOptions.RetryCount == null)
                curOptions.RetryCount = 13;
        }
        public void EncryptPassword()
        {
            if (!String.IsNullOrEmpty(curOptions.PasswordToEncrypt))
            {
                if (!String.IsNullOrEmpty(curOptions.CompanyLOBID))
                {
                    byte[] bytSalt = Innovista.Common.Cryptography.NewKey();
                    string sSalt = System.Text.Encoding.Default.GetString(bytSalt);

                    //byte[] bytCompany = Encoding.ASCII.GetBytes(curOptions.CompanyLOBID);
                    string sEncryptedPassword = Innovista.Common.Cryptography.SimpleEncryptWithPassword(curOptions.PasswordToEncrypt, sSalt);                    

                    //string sEncryptedPassword = hasher.HashPassword(curOptions.PasswordToEncrypt);
                    RealTimeMessagingDataService rtmDS = new RealTimeMessagingDataService(curOptions.ConnectionString);
                    rtmDS.UpdateCompanyPassword(curOptions.CompanyLOBID, sSalt, sEncryptedPassword);
                }
            }
        }
		public bool Process()
		{
            bool bRetVal = true;

            if (curOptions.inputProcessStatus == ProcessStatus.ReadyForMessageFormatting)
			{
                bRetVal = PrepareMessagesForSending();
			}
			if (curOptions.inputProcessStatus == ProcessStatus.ReadyForMessageSending)
			{
                bRetVal = SendMessages();
			}
            if (curOptions.inputProcessStatus == ProcessStatus.ReadyForResponseProcessing)
            {
                bRetVal = ProcessMessageResponses();
            }
            return bRetVal;
        }
        private IMessageDestinationProvider GetProvider(int MessageDestinationID)
		{
			IMessageDestinationProvider oProvider = null;
			switch (MessageDestinationID)
			{
				case (int)SupportedProviderEnum.Availity:
					oProvider = new AvailityMessageDestinationProvider.AvailityProvider();
					break;
				case (int)SupportedProviderEnum.AvailityCore:
					oProvider = new AvailityMessageDestinationProvider.AvailityCoreProvider();
					break;
			}
			return oProvider;
		}
		
        private string GetEZCapAuthNumberFromRawData(string rawMessageData)
        {
            string retVal = string.Empty;
            rawMessageData = rawMessageData.Replace(Environment.NewLine, "");
            string[] rawMessageLines = rawMessageData.Split('~');

            foreach(string msgLine in rawMessageLines)
            {
                string[] msgValues = msgLine.Split('*');
                //looking for TRN line
                if (msgValues[0].Trim().ToUpper() == "TRN")
                {
                    if (msgValues.Length > 4)
                    {
                        //pull the 4 value (indexed by 3) for ezcap number
                        if (!string.IsNullOrEmpty(msgValues[4]))
                            retVal = msgValues[4];

                    } else
                    {
                        //pull the 4 value (indexed by 3) for ezcap number
                        if (!string.IsNullOrEmpty(msgValues[3]))
                            retVal = msgValues[3];
                    }
                }
            }
            return retVal;
        }

		private bool PrepareMessagesForSending()
		{
			RealTimeMessagingDataService objDataService = new RealTimeMessagingDataService(curOptions.ConnectionString);
			IMessageDestinationProvider oProvider = null;
            
            bool bRetVal = true;

			List<RealTimeMessage> MessagesToProcess = objDataService.GetMessageBodyRequestsToBuild(ProcessStatus.ReadyForMessageFormatting, curOptions.CompanyLOBID);
            if (MessagesToProcess != null)
            {
                foreach (RealTimeMessage rtMessage in MessagesToProcess)
                {
                    if (!errMessageDict.ContainsKey(rtMessage.MessageDestID))
                        errMessageDict.Add(rtMessage.MessageDestID, objDataService.GetErrorCodesByMessageDestID(rtMessage.MessageDestID));

                    string ezCapNumber = GetEZCapAuthNumberFromRawData(rtMessage.RawMessageData);
                    if (!string.IsNullOrEmpty(ezCapNumber))
                        rtMessage.EZCapAuthNumber = ezCapNumber;

                    rtMessage.cDevInstanceId = new Guid(curOptions.cDevInstanceID);
                    oProvider = GetProvider(rtMessage.MessageDestID);
                    if (oProvider != null)
                    {
                        try
                        {
                            rtMessage.ProcessStatusStart = SqlHelper.GetCurrentDateTime();
                            objDataService.Update(rtMessage);
                            CompanyMessageDestination messageDestinationInfo = objDataService.GetCompanyMessageDestination(rtMessage);

                            RealTimeMessage updatedMsg = oProvider.FormatMessageBodyForSend(rtMessage, messageDestinationInfo, oProvider.GetProviderRunMode(curOptions.RunMode));
                            if (!updatedMsg.ErrorStatus)
                            {
                                updatedMsg.ProcessStatus = ProcessStatus.MessageFormattingSuccess;
                                updatedMsg.LastStepProcessEnd = SqlHelper.GetCurrentDateTime();
                                objDataService.Update(updatedMsg);
                            }
                            else
                            {
                                updatedMsg.LastStepProcessEnd = SqlHelper.GetCurrentDateTime();
                                objDataService.Update(updatedMsg);
                                if (ReprocessInnovistaHandledError(rtMessage) == false)
                                    bRetVal = false;
                            }
                        }
                        catch (Innovista.Common.Exceptions.DataAccessException ex)
                        {
                            RealTimeMessage exceptionMsg = rtMessage.Copy();
                            exceptionMsg.ErrorStatus = true;
                            exceptionMsg.ErrorCode = RealTimeMessageGenericErrorCodes.DataAccessError;
                            exceptionMsg.ErrorMessage = ex.Message;
                            Exception myException = (Exception)ex;
                            while (myException.InnerException != null)
                            {
                                myException = myException.InnerException;
                                exceptionMsg.ErrorMessage += "|" + myException.Message;
                            }
                            objDataService.Update(exceptionMsg);
                            bRetVal = false;
                        }
                        catch (Innovista.Common.Exceptions.InnovistaException ex)
                        {
                            RealTimeMessage exceptionMsg = rtMessage.Copy();
                            exceptionMsg.ErrorStatus = true;
                            exceptionMsg.ErrorMessage = ex.Message;
                            Exception myException = (Exception)ex;
                            while (myException.InnerException != null)
                            {
                                myException = myException.InnerException;
                                exceptionMsg.ErrorMessage += "|" + myException.Message;
                            }
                            objDataService.Update(exceptionMsg);
                            if (ReprocessInnovistaHandledError(exceptionMsg) == false)
                                bRetVal = false;
                        }
                        catch (Exception ex)
                        {
                            RealTimeMessage exceptionMsg = rtMessage.Copy();
                            exceptionMsg.ErrorStatus = true;
                            exceptionMsg.ErrorCode = RealTimeMessageGenericErrorCodes.UnhandledError;
                            exceptionMsg.ErrorMessage = ex.Message;
                            Exception myException = (Exception)ex;
                            while (myException.InnerException != null)
                            {
                                myException = myException.InnerException;
                                exceptionMsg.ErrorMessage += "|" + myException.Message;
                            }
                            exceptionMsg.LastStepProcessEnd = SqlHelper.GetCurrentDateTime();
                            objDataService.Update(exceptionMsg);
                            bRetVal = false;
                        }
                    }
                }
            }
            return bRetVal;
		}
        private bool SendMessages()
        {
            bool bRetVal = true;
            RealTimeMessagingDataService objDataService = new RealTimeMessagingDataService(curOptions.ConnectionString);
            IMessageDestinationProvider oProvider = null;

            List<RealTimeMessage> MessagesToProcess = objDataService.GetMessagesToProcess(ProcessStatus.ReadyForMessageSending, curOptions.CompanyLOBID);
            if (MessagesToProcess != null)
            {
                foreach (RealTimeMessage rtMessage in MessagesToProcess)
                {
                    if (!errMessageDict.ContainsKey(rtMessage.MessageDestID))
                        errMessageDict.Add(rtMessage.MessageDestID, objDataService.GetErrorCodesByMessageDestID(rtMessage.MessageDestID));

                    try
                    {
                        oProvider = GetProvider(rtMessage.MessageDestID);
                        if (oProvider != null)
                        {
                            rtMessage.ProcessStatusStart = DateTime.Now;
                            rtMessage.cDevInstanceId = new Guid(curOptions.cDevInstanceID);
                            objDataService.Update(rtMessage);

                            CompanyMessageDestination messageDestinationInfo = objDataService.GetCompanyMessageDestination(rtMessage);

                            oProvider.SendMessage(rtMessage, messageDestinationInfo, oProvider.GetProviderRunMode(curOptions.RunMode));
                            if (!rtMessage.ErrorStatus)
                            {
                                rtMessage.ProcessStatus = ProcessStatus.MessageSentSuccessfully;
                                rtMessage.LastStepProcessEnd = SqlHelper.GetCurrentDateTime();
                                objDataService.Update(rtMessage);
                            }
                            else
                            {
                                rtMessage.LastStepProcessEnd = SqlHelper.GetCurrentDateTime();
                                objDataService.Update(rtMessage);
                                if (ReprocessInnovistaHandledError(rtMessage) == false)
                                    bRetVal = false;
                            }                            
                        }
                    }
                    catch (Innovista.Common.Exceptions.DataAccessException ex)
                    {
                        RealTimeMessage exceptionMsg = rtMessage.Copy();
                        exceptionMsg.ErrorStatus = true;
                        exceptionMsg.ErrorCode = RealTimeMessageGenericErrorCodes.DataAccessError;
                        exceptionMsg.ErrorMessage = ex.Message;
                        Exception myException = (Exception)ex;
                        while (myException.InnerException != null)
                        {
                            myException = myException.InnerException;
                            exceptionMsg.ErrorMessage += "|" + myException.Message;
                        }
                        objDataService.Update(exceptionMsg);
                        bRetVal = false;
                    }
                    catch (Innovista.Common.Exceptions.InnovistaException ex)
                    {
                        RealTimeMessage exceptionMsg = rtMessage.Copy();
                        exceptionMsg.ErrorStatus = true;
                        exceptionMsg.ErrorMessage = ex.Message;
                        Exception myException = (Exception)ex;
                        while (myException.InnerException != null)
                        {
                            myException = myException.InnerException;
                            exceptionMsg.ErrorMessage += "|" + myException.Message;
                        }
                        objDataService.Update(exceptionMsg);
                        if (ReprocessInnovistaHandledError(exceptionMsg) == false)
                            bRetVal = false;
                    }
                    catch (Exception ex)
                    {
                        RealTimeMessage exceptionMsg = rtMessage.Copy();
                        exceptionMsg.ErrorStatus = true;
                        exceptionMsg.ErrorCode = RealTimeMessageGenericErrorCodes.UnhandledError;
                        exceptionMsg.ErrorMessage = ex.Message;
                        Exception myException = (Exception)ex;
                        while (myException.InnerException != null)
                        {
                            myException = myException.InnerException;
                            exceptionMsg.ErrorMessage += "|" + myException.Message;
                        }
                        exceptionMsg.LastStepProcessEnd = SqlHelper.GetCurrentDateTime();
                        objDataService.Update(exceptionMsg);
                        bRetVal = false;
                    }
                }
            }
            return bRetVal;
        }
        private bool ProcessMessageResponses()
        {
            bool bRetVal = true;
            RealTimeMessagingDataService objDataService = new RealTimeMessagingDataService(curOptions.ConnectionString);
            IMessageDestinationProvider oProvider = null;

            List<RealTimeMessage> MessagesToProcess = objDataService.GetMessagesToProcess(ProcessStatus.ReadyForResponseProcessing,curOptions.CompanyLOBID);
            if (MessagesToProcess != null)
            {
                foreach (RealTimeMessage rtMessage in MessagesToProcess)
                {
                    if (!errMessageDict.ContainsKey(rtMessage.MessageDestID))
                        errMessageDict.Add(rtMessage.MessageDestID, objDataService.GetErrorCodesByMessageDestID(rtMessage.MessageDestID));

                    try
                    {
                        oProvider = GetProvider(rtMessage.MessageDestID);
                        if (oProvider != null)
                        {
                            rtMessage.ProcessStatusStart = SqlHelper.GetCurrentDateTime();
                            rtMessage.cDevInstanceId = new Guid(curOptions.cDevInstanceID);
                            objDataService.Update(rtMessage);

                            CompanyMessageDestination messageDestinationInfo = objDataService.GetCompanyMessageDestination(rtMessage);
                            List<ErrorMessage> curMessages;

                            errMessageDict.TryGetValue(rtMessage.MessageDestID, out curMessages);
                            RealTimeMessage retMessage = oProvider.ProcessResponse(rtMessage, messageDestinationInfo, oProvider.GetProviderRunMode(curOptions.RunMode), curMessages);
                            if (!retMessage.ErrorStatus)
                            {
                                retMessage.ProcessStatus = ProcessStatus.MessageResponseProcessedSuccessfully;
                                retMessage.LastStepProcessEnd = SqlHelper.GetCurrentDateTime();
                                objDataService.Update(retMessage);
                            }
                            else
                            {
                                retMessage.LastStepProcessEnd = SqlHelper.GetCurrentDateTime();
                                objDataService.Update(retMessage);
                                if (ReprocessInnovistaHandledError(rtMessage) == false)
                                    bRetVal = false;
                            }
                        }
                    }
                    catch (Innovista.Common.Exceptions.DataAccessException ex)
                    {
                        RealTimeMessage exceptionMsg = rtMessage.Copy();
                        exceptionMsg.ErrorStatus = true;
                        exceptionMsg.ErrorCode = RealTimeMessageGenericErrorCodes.DataAccessError;
                        exceptionMsg.ErrorMessage = ex.Message;
                        Exception myException = (Exception)ex;
                        while (myException.InnerException != null)
                        {
                            myException = myException.InnerException;
                            exceptionMsg.ErrorMessage += "|" + myException.Message;
                        }
                        objDataService.Update(exceptionMsg);
                        bRetVal = false;
                    }
                    catch (Innovista.Common.Exceptions.InnovistaException ex)
                    {
                        RealTimeMessage exceptionMsg = rtMessage.Copy();
                        exceptionMsg.ErrorStatus = true;
                        exceptionMsg.ErrorMessage = ex.Message;
                        Exception myException = (Exception)ex;
                        while (myException.InnerException != null)
                        {
                            myException = myException.InnerException;
                            exceptionMsg.ErrorMessage += "|" + myException.Message;
                        }
                        objDataService.Update(exceptionMsg);
                        if (ReprocessInnovistaHandledError(exceptionMsg) == false)
                            bRetVal = false;
                    }
                    catch (Exception ex)
                    {
                        RealTimeMessage exceptionMsg = rtMessage.Copy();
                        exceptionMsg.ErrorStatus = true;
                        exceptionMsg.ErrorCode = RealTimeMessageGenericErrorCodes.UnhandledError;
                        exceptionMsg.ErrorMessage = ex.Message;
                        Exception myException = (Exception)ex;
                        while (myException.InnerException != null)
                        {
                            myException = myException.InnerException;
                            exceptionMsg.ErrorMessage += "|" + myException.Message;
                        }
                        exceptionMsg.LastStepProcessEnd = SqlHelper.GetCurrentDateTime();
                        objDataService.Update(exceptionMsg);
                        bRetVal = false;
                    }
                }
            }
            return bRetVal;
        }
        private bool ReprocessInnovistaHandledError(RealTimeMessage messageWithError)
        {
            bool bRetVal = true;
            RealTimeMessagingDataService objDataService = new RealTimeMessagingDataService(curOptions.ConnectionString);

            try
            {
                if (!errMessageDict.ContainsKey(messageWithError.MessageDestID))
                    errMessageDict.Add(messageWithError.MessageDestID, objDataService.GetErrorCodesByMessageDestID(messageWithError.MessageDestID));
                
                List<ErrorMessage> curMessages = new List<ErrorMessage>();

                if (errMessageDict.TryGetValue(messageWithError.MessageDestID, out curMessages))
                {
                    var queryErrors = from possibleError in curMessages
                                      where possibleError.ErrorCode == messageWithError.ErrorCode
                                      && possibleError.IsRestartable
                                      select possibleError;

                    //if restartable error found, reset message so it can be "reprocessed"
                    if (queryErrors.Count() > 0)
                    {
                        ErrorMessage firstMessage = queryErrors.FirstOrDefault();
                        int iCurRetryCount = objDataService.GetErrorCountFromAuditQueue(messageWithError.ErrorCode, messageWithError.ID);

                        if (iCurRetryCount <= curOptions.RetryCount)
                        {
                            messageWithError.ErrorStatus = false;
                            messageWithError.ErrorCode = string.Empty;
                            messageWithError.ErrorMessage = string.Empty;
                            messageWithError.ProcessStatus = firstMessage.RestartProcessStatus;
                            objDataService.Update(messageWithError);
                        }
                        else
                        {
                            messageWithError.ProcessStatus = ProcessStatus.RetryCountExceeded;
                            objDataService.Update(messageWithError);
                            bRetVal = false;
                        }
                    }
                    else bRetVal = false;
                }
                return bRetVal;
            }
            catch (Exception ex)
            {
                throw new Innovista.Common.Exceptions.InnovistaException("Unhandled error in Error Reprocessing: " + ex.Message, ex);
            }
        }
        //private bool ReprocessInnovistaHandledError(RealTimeMessage messageWithError)
        //{
        //    bool bRetVal = true;
        //    try
        //    {
        //        RealTimeMessagingDataService objDataService = new RealTimeMessagingDataService(curOptions.ConnectionString);
        //        IMessageDestinationProvider oProvider = null;
        //        oProvider = GetProvider(messageWithError.MessageDestID);

        //        if (!errMessageDict.ContainsKey(messageWithError.MessageDestID))
        //            errMessageDict.Add(messageWithError.MessageDestID, objDataService.GetErrorCodesByMessageDestID(messageWithError.MessageDestID));

        //        CompanyMessageDestination messageDestinationInfo = objDataService.GetCompanyMessageDestination(messageWithError);

        //        RealTimeMessage tempMessage = oProvider.ReprocessHandledError(messageWithError, messageDestinationInfo, curOptions.RunMode);
        //        if (tempMessage.ErrorStatus == false)
        //        {
        //            objDataService.Update(tempMessage);
        //        }
        //        else bRetVal = false;
        //        return bRetVal;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Innovista.Common.Exceptions.InnovistaException("Unhandled error in Error Reprocessing: " + ex.Message, ex);
        //    }
        //}
    }
}
