﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Innovista.Common.Exceptions
{
	/// <summary>
	/// Exception for invalid input, typically on client-side
	/// </summary>
	[Serializable]
	public class InvalidInputException : InnovistaException
	{
		public InvalidInputException()
			: base()
		{
		}
		public InvalidInputException(string Message)
			: base(Message)
		{
		}
		public InvalidInputException(string Message, Exception inner)
			: base(Message, inner)
		{
		}
		public InvalidInputException(SerializationInfo Info, StreamingContext context)
			: base(Info, context)
		{
		}
	}
}
