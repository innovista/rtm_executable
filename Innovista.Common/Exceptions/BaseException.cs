﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Innovista.Common.Exceptions
{
	/// <summary>
	/// Custom base exception class for TruArx.Utils
	/// </summary>
	[Serializable]
	public class InnovistaException : ApplicationException
	{
		#region minimal base implementation
		public InnovistaException()
		{
		}
		public InnovistaException(string message)
			: base(message)
		{
		}
		public InnovistaException(string message, Exception inner)
			: base(message, inner)
		{
		}
		#endregion

		#region ISerializable Implementation
		protected InnovistaException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
		/// <summary>
		/// override base serialization method to add credential information
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			//info.AddValue("ErrorCode", errorcode);
			//call the base exception class to ensure proper serialization
			base.GetObjectData(info, context);
		}
		#endregion

	}
}
