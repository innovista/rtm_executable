﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using Innovista.Common.Exceptions;

namespace Innovista.Common
{
	public static class InputValidation
	{

		#region Input Sanitizing

		public static bool IsXSSVulnerable(string Content)
		{
			return Regex.IsMatch(Content, @"[\[\]\^|\?\*\\~`\$!%=+{}<>:;]");
		}

		public static bool IsXSSVulnerable(Stream FileStream)
		{
			byte[] oBuffer = new byte[FileStream.Length];
			FileStream.Read(oBuffer, 0, (int)FileStream.Length);
			string Content = Encoding.Default.GetString(oBuffer);
			return Regex.IsMatch(Content, @"[\[\]\^|\?\*\\~`\$!%=+{}<>:;]");
		}

		public static bool IsAttachFileNameValid(string FileName)
		{
			//allows file name up to 50 Characters long with specific extensions.
			return Regex.IsMatch(FileName, @"^[a-zA-Z0-9\s-_]{1,50}\.(pdf|png|bmp|jpg|jpeg|tif|gif|PDF|PNG|BMP|JPG|JPEG|TIF|GIF)$");
		}
		public static bool IsAttachContentTypeValid(string contentType)
		{
			//validates content-type are image or PDF
			return Regex.IsMatch(contentType.ToLower(), @"^((image\/[A-Za-z]*)|(application\/(x-){0,1}pdf))$");
		}
		public static String StripXSS(string Content, bool IncludeCharacters)
		{
			Regex regex = new Regex(@"<script[^>]*>[^>]*>|alert\(|onerror|javascript:");//removes <script>code...</script>
			string sanitizedContent = regex.Replace(Content, "");

			if (IncludeCharacters)
				return Regex.Replace(sanitizedContent, @"[\[\]\^|\?\*\(\)\\~`\$!#%=+{}<>:;]", string.Empty);
			else
				return sanitizedContent;
		}

		/// <summary>
		/// cleans up a username that does not comply with the I9 policy
		/// </summary>
		/// <param name="sUsername">username</param>
		public static String SanitizeUsername(string sUsername)
		{
			Regex regex = new Regex(@"[^a-zA-Z0-9_\-.@]"); //removes anything but alphanumeric characters, underscore (_), hyphen (-), period (.) or at symbol (@)
			return regex.Replace(sUsername, "");
		}

		#endregion

		public const string VALID_STRING_EXPRESSION = "^[a-zA-Z'./s]{1,40}$";
		#region Email Pattern Expression
		//Allows            joe@home.com | joe.bob@home.com | joe@his.home.com | joe@his.home.place | joe@home.org | joe@joebob.name 
		//                  joe$@bob.com | joe+bob@bob.com | joe'bob@home.com
		//Doesn't allow     joe | joe@home | joe@bob.home | joe-bob[at]home.com | joe.@bob.com | .joe@bob.com 
		//                  joe<>bob@bob.come | joe&bob@bob.com | ~joe@bob.com
		public const string VALID_EMAIL_STRING_EXPRESSION = @"^(([^<>()[\]\\.,;:\s@\""]+(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
			   + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$";
		#endregion Email Pattern Expression

		#region Name Pattern Expression
		//Allows            Sameul | O'Conner | Mary-Kate
		//Doesn't allow     David Bugel | Robert1 | Robert M. Larry
		public const string VALID_NAME_STRING_EXPRESSION = @"^[a-zA-Z]+(([\'\,\.\-][a-zA-Z])?[a-zA-Z]*)*$";
		#endregion Name Pattern Expression      

		#region Alpha Numeric Pattern Expression
		//Allows            2222 Mock St. | 1, A St. | 555-1212
		//Doesn't allow     [A Street] | (3 A St.) | {34 C Ave.}
		public const string VALID_ALPHA_NUMERIC_STRING_EXPRESSION = @"^[a-zA-Z0-9\s.,-\-]+$";
		#endregion Alpha Numeric Pattern Expression

		#region Alpha Numeric Special Pattern Expression
		//Allows            2222 Mock St. | 1 A St. | 555-1212 | $5,000,000 | AT&T | Sam's Club | A (ABC) 
		//Doesn't allow     BIG CA$$H | -- delete * from ABC | O`malley | Oh! 
		public const string VALID_ALPHA_NUMERIC_SPECIAL_STRING_EXPRESSION = @"^([\w\._+-;,%&$()']|\s)*$";
		#endregion Alpha Numeric Special Pattern Expression

		#region Integer Value Pattern Expression
		//Allows            1234 
		//Doesn't allow     1231ABC
		public const string VALID_INTERGER_EXPRESSION = @"^\d*$";
		#endregion International Phone Number Pattern Expression

		public static bool IsValidString(string inputString, string regularExpression)
		{
			Regex expression = new Regex(regularExpression);
			if (expression.IsMatch(inputString))
				return true;
			else
				return false;
		}
		public static IList<Guid> ValidateInputMultiGuid(string inputString, System.Char delimiter)
		{
			string tempString = ValidateInputMultiGuidAsString(inputString);
			string[] tempArray = tempString.Split(new char[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);

			IList<Guid> retVal = new List<Guid>();
			foreach (string tempVal in tempArray)
				retVal.Add(new Guid(tempVal));

			return retVal;
		}
		public static String ValidateInputMultiGuidAsString(string inputString)
		{
			string tempInput;
			int guidLength = Guid.NewGuid().ToString().Length;

			//for multiple guids, we add 1 char for delimiter, so length should be multiple of
			//guidlength + 1
			guidLength += 1;
			tempInput = Regex.Replace(inputString, @"[^\w\-~,]", "");

			//check length
			if ((tempInput.Length % guidLength) == 0)
				return tempInput;
			else
				throw new InvalidInputException("Invalid Guid String.");
		}
		public static String ValidateInputGuidAsString(string inputString)
		{
			string tempInput;

			tempInput = Regex.Replace(inputString, @"[^\w\-]", "");
			//check length
			if (IsValidStringLength(tempInput, Guid.NewGuid().ToString().Length))
				return tempInput;
			else
				throw new InvalidInputException("Invalid Guid.");
		}

		public static Guid ValidateInputGuid(string inputString)
		{
			string tempInput;

			tempInput = Regex.Replace(inputString, @"[^\w\-]", "");
			//check length
			if (IsValidStringLength(tempInput, Guid.NewGuid().ToString().Length))
				return new Guid(tempInput);
			else
				throw new InvalidInputException("Invalid Guid.");
		}

		public static bool IsValidStringLength(string inputString, Int32 stringLength)
		{
			if (inputString.Length <= stringLength)
				return true;
			else
				return false;
		}

		public static string ValidateRequiredFieldString(string fieldName, string fieldValue, Int32 stringLength)
		{
			string _temp = ValueTypeConstants.NullString;

			if (String.IsNullOrEmpty(fieldValue))
				throw new InvalidInputException(string.Format("{0} is a required field.", fieldName));

			_temp = fieldValue.Trim();
			if (_temp.Length > stringLength)
				throw new InvalidInputException(string.Format("{0} needs to be less than {1} character(s)", fieldName, stringLength.ToString()));

			return _temp;
		}

		public static int ValidateRequiredFieldInteger(string fieldName, int fieldValue)
		{
			if (fieldValue <= 0)
				throw new InvalidInputException(string.Format("{0} is not valid", fieldName));

			return fieldValue;
		}

		public static string ValidateInputString(string inputString, Int32 stringLength)
		{
			string _temp = null;

			_temp = inputString;
			if (!String.IsNullOrEmpty(_temp))
			{
				_temp = _temp.Trim();
				if (IsValidStringLength(_temp, stringLength))
					return _temp;
				else
					throw new InvalidInputException("Input String Too Long - " + _temp);
			}
			return ValueTypeConstants.NullString;
		}

		public static string ValidateInputString(string inputString, Int32 stringLength, string regExpression)
		{
			string _temp = null;

			try
			{
				_temp = inputString;
				_temp = ValidateInputString(_temp, stringLength);

				if (!IsValidString(_temp, regExpression))
					throw new InvalidInputException();
				return _temp;
			}
			catch (Exception ex)
			{
				throw new InvalidInputException("Invalid Input String. " + ex.Message);
			}
		}

		public static Boolean ValidateInputBoolean(string inputString)
		{
			Boolean _tempBool = false;
			string _temp = null;

			_temp = inputString;

			try
			{
				if ((_temp.StartsWith("1")) || _temp.StartsWith("0"))
				{
					_temp = ValidateInputString(inputString, 1);
					if (_temp == "1")
						_tempBool = true;
					else
						_tempBool = false;
				}
				else
					if ((_temp.ToUpper().StartsWith("T")) || (_temp.ToUpper().StartsWith("F")))
				{
					_temp = ValidateInputString(inputString, 5);
					_tempBool = Boolean.Parse(inputString);
				}
			}
			catch (Exception ex)
			{
				throw new InvalidInputException("Invalid Input Boolean.", ex);
			}
			return _tempBool;
		}

		public static Int32 ValidateInputInt32(string inputString, int stringLength)
		{
			Int32 _tempInt = ValueTypeConstants.NullInt;
			string _temp = null;

			try
			{
				_temp = ValidateInputString(inputString, stringLength);
				_tempInt = ValidateInputInt32(_temp);
			}
			catch (Exception ex)
			{
				throw new InvalidInputException("Invalid Input Integer.", ex);
			}
			return _tempInt;
		}

		public static Int32 ValidateInputInt32(string inputString)
		{
			Int32 _tempInt = ValueTypeConstants.NullInt;

			try
			{
				_tempInt = Int32.Parse(inputString);
			}
			catch (Exception ex)
			{
				throw new InvalidInputException("Invalid Input Integer.", ex);
			}
			return _tempInt;
		}

		public static Double ValidateInputDouble(string inputString)
		{
			Double _tempDouble = ValueTypeConstants.NullDouble;

			try
			{
				_tempDouble = Double.Parse(inputString);
			}
			catch (Exception ex)
			{
				throw new InvalidInputException("Invalid Input Double.", ex);
			}
			return _tempDouble;
		}

		public static Int32 ValidateInputInt32MaxValue(string inputString, Int32 maxIntValue)
		{
			Int32 _tempInt = ValueTypeConstants.NullInt;

			try
			{
				_tempInt = ValidateInputInt32(inputString);
				if (_tempInt > maxIntValue)
					throw new InvalidInputException("Integer Value Too Large.");
			}
			catch (Exception ex)
			{
				throw new InvalidInputException("Invalid Input Integer.", ex);
			}
			return _tempInt;
		}
		public static String EmptyIfNull(string inputString)
		{
			try
			{
				return (inputString != null) ? inputString : String.Empty;
			}
			catch
			{
				return String.Empty;
			}
		}
		public static int NegativeIfNull(Nullable<int> inputInt)
		{
			try
			{
				return inputInt.GetValueOrDefault(-99);
			}
			catch
			{
				return -99;
			}
		}
		public static Boolean FalseIfNull(Boolean? inputBool)
		{
			try
			{
				return inputBool.GetValueOrDefault(false);
			}
			catch
			{
				return false;
			}
		}
		public static Boolean TrueIfNull(Boolean? inputBool)
		{
			try
			{
				return inputBool.GetValueOrDefault(true);
			}
			catch
			{
				return true;
			}
		}

		public static string ValidateUserPassword(string strPassword)
		{
			try
			{
				if (!Regex.IsMatch(strPassword, @"^.*(?=.{6,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$"))
				{
					throw new InvalidInputException("Invalid User Password. Password must be at least six characters long and contain at least one uppercase letter, one lowercase letter, and one number");
				}
			}
			catch (Exception ex)
			{
				throw new InvalidInputException("Invalid User Password. Password must be at least six characters long and contain at least one uppercase letter, one lowercase letter, and one number", ex);
			}
			return strPassword;
		}

		public static bool IncludeCharacters { get; set; }
	}
}
