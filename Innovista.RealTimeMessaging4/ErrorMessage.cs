﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innovista.RealTimeMessaging
{
    public class ErrorMessage
    {
        public int ID { get; set; }
        public int? MessageDestID { get; set; }
        public string ErrorCode { get; set; }
        public string Description { get; set; }
        public bool IsRestartable { get; set; }
        public string RestartProcessStatus { get; set; }
    }
}
