﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innovista.RealTimeMessaging
{
	public interface IMessageDestinationProvider
	{
		string GetProviderRunMode(string runMode);
		RealTimeMessage FormatMessageBodyForSend(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode);
		RealTimeMessage SendMessage(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode);
        RealTimeMessage ProcessResponse(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode, List<ErrorMessage> dbErrors);
        //RealTimeMessage ReprocessHandledError(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode);
    }
}
