﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Innovista.Common;

namespace Innovista.RealTimeMessaging
{
	public class RealTimeMessage
	{
		public int ID { get; set; }
		public Guid cDevInstanceId { get; set; }
		public int CompanyLOBID { get; set; }
		public int MessageDestID { get; set; }
		public string EDIMessageType { get; set; }
		public string TranType { get; set; }
		public string TranTypeID { get; set; }
		public string RawMessageData { get; set; }
		public string EZCapAuthNumber { get; set; }
		public string HPAuthNumber { get; set; }
		public string ProcessStatus { get; set; }
		public string MessageBody { get; set; }
		public string MessageResponse { get; set; }
		public bool ErrorStatus { get; set; }
		public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime ProcessStatusStart { get; set; }
		public DateTime LastStepProcessEnd { get; set; }


		public RealTimeMessage()
		{
			initialize();
		}
		private void initialize()
		{
			ID = 0;
			cDevInstanceId = Guid.Empty;
			CompanyLOBID = 0;
			MessageDestID = 0;
			HPAuthNumber = string.Empty;
			EZCapAuthNumber = string.Empty;
			EDIMessageType = string.Empty;
			TranType = String.Empty;
			TranTypeID = string.Empty;
			RawMessageData = string.Empty;
			ProcessStatus = string.Empty;
			MessageBody = string.Empty;
			MessageResponse = string.Empty;
			ErrorCode = string.Empty;
			ErrorStatus = false;
            ErrorMessage = string.Empty;
			ProcessStatusStart = ValueTypeConstants.NullDateTime;
			LastStepProcessEnd = ValueTypeConstants.NullDateTime;
		}
        public RealTimeMessage Copy()
        {
            RealTimeMessage newMessage = new RealTimeMessage();
            newMessage.ID = this.ID;
            newMessage.cDevInstanceId = this.cDevInstanceId;
            newMessage.CompanyLOBID = this.CompanyLOBID;
            newMessage.MessageDestID = this.MessageDestID;
            newMessage.HPAuthNumber = this.HPAuthNumber;
            newMessage.EZCapAuthNumber = this.EZCapAuthNumber;
            newMessage.EDIMessageType = this.EDIMessageType;
            newMessage.TranType = this.TranType;
            newMessage.TranTypeID= this.TranTypeID;
            newMessage.RawMessageData = this.RawMessageData;
            newMessage.ProcessStatus = this.ProcessStatus;
            newMessage.MessageBody = this.MessageBody;
            newMessage.MessageResponse = this.MessageResponse;
            newMessage.ErrorCode = this.ErrorCode;
            newMessage.ErrorStatus = this.ErrorStatus;
            newMessage.ErrorMessage = this.ErrorMessage;
            newMessage.ProcessStatusStart = this.ProcessStatusStart;
            newMessage.LastStepProcessEnd = this.LastStepProcessEnd;
            return newMessage;
        }
	}
}
