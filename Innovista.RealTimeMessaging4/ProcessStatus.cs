﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innovista.RealTimeMessaging
{
	public sealed class ProcessStatus
	{
		public static string ReadyForMessageFormatting = "INI_TRXN_SUCCESS";
		public static string MessageFormattingSuccess = "MSG_BODY_GEN_SUCCESS";

		public static string ReadyForMessageSending = "MSG_BODY_GEN_SUCCESS";
		public static string MessageSentSuccessfully = "MSG_SENT";
        //public static string MessageReceivedSuccessfully = "MSG_RECEIVED";
        public static string ReadyForResponseProcessing = "MSG_SENT";
        public static string MessageResponseProcessedSuccessfully = "MSG_PROCESSED";
        public static string MessageComplete = "MSG_COMPLETE";
        public static string RetryCountExceeded = "EXCEEDED_RETRY_COUNT";
    }
}
