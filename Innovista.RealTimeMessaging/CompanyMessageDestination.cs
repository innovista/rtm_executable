﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innovista.RealTimeMessaging
{
	public class CompanyMessageDestination
	{
		public int DestID { get; set; }
		public int CompanyLOBID { get; set; }
		public string Endpoint { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string DestName { get; set; }
		public string CommType { get; set; }
		public string AuthorizationType { get; set; }
		public string ClientID { get; set; }
		public string DestClientID { get; set; }
		public string SenderName { get; set; }

		public CompanyMessageDestination()
		{
			DestID = 0;
			CompanyLOBID = 0;
			Endpoint = string.Empty;
			UserName = string.Empty;
			Password = string.Empty;
			DestName = string.Empty;
			CommType = string.Empty;
			AuthorizationType = string.Empty;
			ClientID = string.Empty;
			DestClientID = string.Empty;
			SenderName = string.Empty;
		}
	}
}
