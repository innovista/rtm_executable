﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innovista.RealTimeMessaging
{
	public sealed class ProcessRunMode
	{
		public static string Test = "T";
		public static string Production = "P";
	}
}
