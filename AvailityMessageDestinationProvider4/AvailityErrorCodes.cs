﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvailityMessageDestinationProvider
{
    public sealed class AvailityErrorCodes
    {
        public const string VT01_001_MissingISADetailsReq = "VT01_001";
        public const string VT01_002_MissingGSDetailsReq = "VT01_002";
        public const string VT04_003_MissingBHTDetailsReq = "VT04_003";
        public const string VT04_004_MissingGEDetailsReq = "VT04_004";
        public const string VT04_005_MissingIEAADetailsReq = "VT04_005";
        public const string VT04_006_MissingNM1DetailsReq = "VT04_006";
        public const string VT06_007_MissingISADetailsRsp = "VT06_007";
        public const string VT06_008_MissingGSDetailsRsp = "VT06_008";
        public const string VT06_009_MissingBHTDetailsRsp = "VT06_009";
        public const string VT06_010_MissingGEDetailsRsp = "VT06_010";
        public const string VT06_011_MissingIEADetailsRsp = "VT06_011";

        public const string VT01_012_NoTransactionReq = "VT01_012";
        public const string VT06_013_NoTransactionRsp = "VT06_013";
        public const string VT01_014_InvalidXMLReq = "VT01_014";
        public const string VT01_015_MissingReqFieldXMLReq = "VT01_015";
        public const string VT01_016_InvalidSessionIDFormatReq = "VT01_016";
        public const string VT01_017_InactiveUserReq = "VT01_017";
        public const string VT01_018_InvalidUserISAReq = "VT01_018";
        public const string VT01_019_InvalidPassISAReq = "VT01_019";
        public const string VT01_020_MissingUserOrSessionReq = "VT01_020";

        public const string VT01_021_InvalidSessionIDReq = "VT01_021";
        public const string VT01_022_ExpiredSessionIDReq = "VT01_022";
        public const string VT01_023_InvalidVendorReq = "VT01_023";
        public const string VT01_024_InvalidTransactionXMLReq = "VT01_024";
        public const string VT01_025_InvalidRcvrTrxnCombo = "VT01_025";
        public const string VT01_026_InvalidHIPAAVersReq = "VT01_026";
        public const string VT01_027_NoX12TrxnInBodyReq = "VT01_027";
        public const string VT01_028_InvalidMsgFormatReq = "VT01_028";

        public const string VT02_029_VendorPoolFull = "VT02_029";
        public const string VT02_030_MediatorPoolFull = "VT02_030";
        public const string VT03_031_TooManyTrxn = "VT03_031";
        public const string VT04_032_HIPAAValidReq = "VT04_032";
        public const string VT06_033_HIPAAValidRsp = "VT06_033";
        public const string VT04_034_InvalidSubscriberID = "VT04_034";
        public const string VT05_035_UnableToConnect = "VT05_035";
        public const string VT06_036_InvalidPayerResp = "VT06_036";
        public const string VT07_037_RetryLater = "VT07_037";
        public const string VT01_038_MultipleSTSE = "VT01_038";
        public const string VT04_039_Multi278Error = "VT04_039";
        public const string VT06_040_InvalidPayerResp = "VT06_040";
    }
}
