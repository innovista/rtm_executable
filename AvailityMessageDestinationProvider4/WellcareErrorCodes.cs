﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvailityMessageDestinationProvider
{
    public sealed class WellcareErrorCodes
    {
        public const string WC_999_RequestNotApproved = "WC_999";
        public const string WC_15_RequiredDataMessing = "15";
        public const string WC_33_InputErrors = "33";
        public const string WC_42_NoResponse = "42";
        public const string WC_43_InvalidProviderIdent = "43";
        public const string WC_44_InvalidProviderName= "44";
        public const string WC_46_InvalidProviderPhone = "46";
        public const string WC_51_ProviderNotOnFile = "51";
        public const string WC_57_InvalidDatesOfService = "57";
        public const string WC_58_InvalidDateOfBirth = "58";
        public const string WC_64_InvalidPatientID = "64";
        public const string WC_65_InvalidPatientName = "65";
        public const string WC_66_InvalidPatientGender = "66";
        public const string WC_71_PatientBirthDateDoesNotMatch = "71";
        public const string WC_75_SubscriberNotFound = "75";
        public const string WC_95_PatientNotEligible = "95";
        public const string WC_AM_InvalidAdmissionDate= "AM";
        public const string WC_AF_InvalidDiagnosisCode= "AF";
        public const string WC_T4_PayerMissing= "T4";
    }
}
