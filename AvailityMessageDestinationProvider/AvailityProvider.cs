﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Net;
using System.IO;

using Innovista.RealTimeMessaging;

namespace AvailityMessageDestinationProvider
{
	public class AvailityProvider : IMessageDestinationProvider
	{
		public AvailityProvider()
		{
		}

		public RealTimeMessage FormatMessageBodyForSend(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode)
		{
			string ansiResponse = "";
			try
			{
				var xd = new XmlDocument();
				xd.LoadXml("<?xml version=\"1.0\"?><ENVELOPE/>");
				var xnHeader = xd.DocumentElement.AppendChild(xd.CreateElement("HEADER"));
				xnHeader.AppendChild(xd.CreateElement("TranCode")).InnerText = GetTranCode(oMessageToFormat.EDIMessageType);
				xnHeader.AppendChild(xd.CreateElement("MessageFormat")).InnerText = GetTranMessageFormat();
				xnHeader.AppendChild(xd.CreateElement("Sender")).InnerText = messageDestinationInfo.SenderName;
				xnHeader.AppendChild(xd.CreateElement("Receiver")).InnerText = GetAvailityReceiver();
				xnHeader.AppendChild(xd.CreateElement("Session"));
				xnHeader.AppendChild(xd.CreateElement("ProviderOfficeNbr")).InnerText = "FOX";
				xnHeader.AppendChild(xd.CreateElement("ProviderTransID")).InnerText = oMessageToFormat.EZCapAuthNumber;
				xd.DocumentElement.AppendChild(xd.CreateElement("BODY")).InnerText = GetX12MessageWrapper(oMessageToFormat, messageDestinationInfo, runMode);
				ansiResponse = xd.OuterXml.Replace("<Session />", "<Session></Session>");
				oMessageToFormat.MessageBody = ansiResponse;
				if (oMessageToFormat.RawMessageData == string.Empty)
				{
					oMessageToFormat.MessageBody = string.Empty;
					oMessageToFormat.ErrorStatus = true;
					oMessageToFormat.ErrorCode = RealTimeMessageGenericErrorCodes.NoRawMessageData;
				}

				return oMessageToFormat;
			}
			catch (Exception ex)
			{
				throw new Innovista.Common.Exceptions.InnovistaException("An unexpected error occurred.", ex);
			}
		}
		private string GetX12MessageWrapper(RealTimeMessage oMessage, CompanyMessageDestination compInfo, string runMode)
		{
			string sISAHeader= string.Empty;
			string sGSHeader= string.Empty;
			string sGEFooter = string.Empty;
			string sIEAFooter = string.Empty;
			DateTime dtNow = DateTime.UtcNow;
			string sDateNow = GetInterchangeDate(dtNow, 2);
			string sTimeNow = GetInterchangeTime(dtNow);

			//ISA*03*S269359  *01*fkehMAJIiK*ZZ*AV09311993     *01*030240928      *110301*2144*^*00501*000000002*0*T*:~
			//sISAHeader = String.Format("ISA*03*{0}*01*{1}*ZZ*AV09311993     *01*030240928      *{2}*{3}*^*00501*{4}*0*{5}*:", GetUserNamePlaceholder(), GetPasswordPlaceholder(), sDateNow, sTimeNow, oMessage.EZCapAuthNumber, runMode);
			//sISAHeader = String.Format("ISA*03*{0}*01*{1}*ZZ*AV09311993     *01*030240928      *{2}*{3}*^*00501*{4}*0*{5}*:", GetUserNamePlaceholder(), GetPasswordPlaceholder(), sDateNow, sTimeNow, oMessage.EZCapAuthNumber, runMode);
			sISAHeader = String.Format("ISA*03*{0}*01*{1}*ZZ*AV09311993     *01*593547616      *{2}*{3}*^*00501*{4}*0*{5}*:", GetUserNamePlaceholder(), GetPasswordPlaceholder(), sDateNow, sTimeNow, oMessage.EZCapAuthNumber, runMode);
			sISAHeader = "ISA*03*S269359  *01*fkehMAJIiK*01*AV09311993     *ZZ*593547616      *191114*1014*^*00501*000000003*0*T*:";
			//ISA*00*          *00*          *01*OSHMI          *01*278TEST        *191112*0959*^*00501*000000017*1*T*:~
			//sISAHeader = String.Format("ISA*00*{0}*00*{1}*01*OSHMI          *01*278TEST        *{2}*{3}*^*00501*{4}*1*{5}*:", GetUserNamePlaceholder(), GetPasswordPlaceholder(), sDateNow, sTimeNow, "000000017", runMode);



			DateTime dtGSNow = DateTime.UtcNow;
			string sGSDateNow = GetInterchangeDate(dtGSNow, 4);
			string sGSTimeNow = GetInterchangeTime(dtGSNow);
			//GS*HI*AV01101957*030240928*20110301*214457*2*X*005010X217~		
			sGSHeader = String.Format("GS*HI*AV01101957*593547616*{0}*{1}*{2}*X*005010X217", sGSDateNow, sGSTimeNow, oMessage.EZCapAuthNumber);
			sGSHeader = "GS*HI*AV09311993*593547616*20191114*1014*3*X*005010X217~";
			//GS*HI*OSHMI*278TEST*20191112*0959*17*X*005010X217~
			//sGSHeader = String.Format("GS*HI*OSHMI*278TEST*{0}*{1}*{2}*X*005010X217", sGSDateNow, sGSTimeNow, "17");

			//sGEFooter = String.Format("GE*1*{0}", oMessage.TranTypeID);
			sGEFooter = String.Format("GE*1*{0}", oMessage.EZCapAuthNumber);
			sGEFooter = "GE*1*3";
			//sIEAFooter = String.Format("IEA*1*{0}", oMessage.EZCapAuthNumber);
			sIEAFooter = String.Format("IEA*1*{0}", oMessage.EZCapAuthNumber);
			return String.Format("{0}~{1}~{2}~{3}~{4}~", sISAHeader, sGSHeader, oMessage.RawMessageData, sGEFooter, sIEAFooter);
		}
		#region HelperMethods
		private string GetInterchangeDate(DateTime dtDate, int numChars)
		{
			string sNow = string.Empty;
			if (numChars <= 2)
				sNow = dtDate.Year.ToString().Substring(2, 2);
			else if (numChars >= 4)
				sNow = dtDate.Year.ToString();

			string sMonthPart = dtDate.Month.ToString();
			if (sMonthPart.Length == 1)
				sMonthPart = "0" + sMonthPart;
			sNow += sMonthPart;
			sMonthPart = dtDate.Day.ToString();
			if (sMonthPart.Length == 1)
				sMonthPart = "0" + sMonthPart;
			sNow += sMonthPart;
			return sNow;
		}
		private string GetInterchangeTime(DateTime dtDate)
		{
			string sNow = string.Empty;
			string sTimePart = dtDate.Hour.ToString();
			if (sTimePart.Length == 1)
				sTimePart = "0" + sTimePart;
			sNow += sTimePart;
			sTimePart = dtDate.Minute.ToString();
			if (sTimePart.Length == 1)
				sTimePart = "0" + sTimePart;
			sNow += sTimePart;
			return sNow;
		}
		private string GetTranCode(string EDIMessageType)
		{
			string retCode = String.Empty;
			if (EDIMessageType.ToUpper() == "EDI278")
				retCode = "278";

			return retCode;
		}
		private string GetTranMessageFormat()
		{
			return "X12";
		}
		private string GetUserNamePlaceholder()
		{
			return "[UserNameTBD]";
		}
		private string GetPasswordPlaceholder()
		{
			return "[PasswordTBD]";
		}
		private string GetAvailityReceiver()
		{
			return "AVAILITY";
		}
		protected virtual void SendRequest(HttpWebRequest request, string payload)
		{
			using (var writer = new StreamWriter(request.GetRequestStream(), Encoding.Default))
			{
				writer.Write(payload);
			}
		}
		protected virtual string GetResponse(HttpWebRequest request)
		{
			var response = (HttpWebResponse)request.GetResponse();
			using (var reader = new StreamReader(response.GetResponseStream()))
			{
				return reader.ReadToEnd();
			}
		}

		#endregion


		private void SendMessage()
		{
			//var httpRequest = (HttpWebRequest)WebRequest.Create(Url);
			//httpRequest.Method = "POST";
			//httpRequest.ContentType = "application/x-www-form-urlencoded";
			//SendRequest(httpRequest, xmlRequest);
			//var xmlResponse = GetResponse(httpRequest);
			//if (!string.IsNullOrEmpty(xmlResponse))
			//{
			//	xd.LoadXml(xmlResponse);
			//	var xnTemp = xd.DocumentElement.SelectSingleNode("BODY");
			//	if (xnTemp != null)
			//	{
			//		var xnError = xnTemp.SelectSingleNode("Interchange/Error");
			//		if (xnError != null)
			//		{
			//			ansiResponse = xnError.InnerText;
			//		}
			//		else
			//		{
			//			ansiResponse = xnTemp.InnerText;
			//		}
			//	}
			//}
		}

		public RealTimeMessage SendMessage(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode)
		{
			try
			{
				RealTimeMessage updatedMessage = AddCredentials(oMessageToFormat, messageDestinationInfo);
				var httpRequest = (HttpWebRequest)WebRequest.Create(messageDestinationInfo.Endpoint);
				
				httpRequest.Method = "POST";
				httpRequest.ContentType = "application/x-www-form-urlencoded";
				SendRequest(httpRequest, updatedMessage.MessageBody);
				//reset messageboody back to original so that it is not stored in DB when updated.
				//updatedMessage.MessageBody = oMessageToFormat.MessageBody;
				var xmlResponse = GetResponse(httpRequest);
				if (!string.IsNullOrEmpty(xmlResponse))
				{
					XmlDocument xd = new XmlDocument();
					xd.LoadXml(xmlResponse);
					var xnTemp = xd.DocumentElement.SelectSingleNode("BODY");
					if (xnTemp != null)
					{
						var xnError = xnTemp.SelectSingleNode("Interchange/Error");
						if (xnError != null)
						{
							updatedMessage.ErrorStatus = true;
							updatedMessage.ErrorCode = RealTimeMessageGenericErrorCodes.GeneralApplicationResponseError;
							updatedMessage.MessageResponse = xmlResponse;
						}
						else
						{
							updatedMessage.ErrorStatus = false;
							updatedMessage.ErrorCode = string.Empty;
							updatedMessage.MessageResponse = xmlResponse;
						}
					}
				}
				return updatedMessage;
			}
			catch (Exception ex)
			{
				throw new Innovista.Common.Exceptions.InnovistaException("An unexpected error occurred.", ex);
			}
		}
		private RealTimeMessage AddCredentials(RealTimeMessage oMessageToSend, CompanyMessageDestination messageDestinationInfo)
		{
			try
			{
				RealTimeMessage updatedMessage = oMessageToSend;
				string updatedMessageBody = oMessageToSend.MessageBody;
				
				var xd = new XmlDocument();
				xd.LoadXml(updatedMessageBody);
				XmlNodeList xeBody = xd.GetElementsByTagName("BODY");
				if (xeBody == null)
					throw new Innovista.Common.Exceptions.InnovistaException("Unable to find BODY xml node");
				if (xeBody.Count != 1)
					throw new Innovista.Common.Exceptions.InnovistaException("More or less than one BODY node found.");
				XmlElement xmlBody = (XmlElement)xeBody.Item(0);


				string sUserName = messageDestinationInfo.UserName;
				if (sUserName.Length > 10)
					throw new Innovista.Common.Exceptions.InnovistaException("Username credential too long.");

				while (sUserName.Length < 10)
				{
					sUserName += " ";
				}
				xmlBody.InnerText = xmlBody.InnerText.Replace(GetUserNamePlaceholder(), sUserName);

				string sUserPass = messageDestinationInfo.Password;
				if (sUserPass.Length > 10)
					throw new Innovista.Common.Exceptions.InnovistaException("UserPassword credential too long.");

				while (sUserPass.Length < 10)
				{
					sUserPass += " ";
				}
				xmlBody.InnerText = xmlBody.InnerText.Replace(GetPasswordPlaceholder(), sUserPass);
				updatedMessage.MessageBody = xd.OuterXml;

				return updatedMessage;
			}
			catch (Exception ex)
			{
				throw new Innovista.Common.Exceptions.InnovistaException("Unable to add credential to message body", ex);
			}
		}

		public string GetProviderRunMode(string runMode)
		{
			string sRetVal = string.Empty;
			if (runMode == ProcessRunMode.Test)
				sRetVal = "T";
			else if (runMode == ProcessRunMode.Production)
				sRetVal = "P";
			return sRetVal;
		}
	}
}
